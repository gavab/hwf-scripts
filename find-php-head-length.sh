#!/bin/bash

###
#
# Author	Gavin Abson
# Created	2017-12-05
# Modified	2017-12-05
#
# This script will search for PHP files and output a list, sorted by
# the size of their first few lines.
# The more characters in the first few lines, the more suspicious the 
# file.
#
###

find -type f -iname '*.php' -exec sh -c 'echo "`head -n5 "{}" | wc -c` {}"' \; | sort -n

