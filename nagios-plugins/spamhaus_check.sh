#!/bin/bash

# Get the reversed IP
REVERSEIP=`dig +short a $1 | awk -F. '{print $4"."$3"." $2"."$1}'`
RESULT=`dig +short $REVERSEIP.zen.spamhaus.org`

# If the result has 0 length then everything is fine
if [ `echo $RESULT | wc -c` -eq 0 ]
then
  echo "Not listed\n"
  exit 0
else
  echo "Found in blacklist\n"
  exit 2
fi
