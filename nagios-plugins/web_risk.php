#!/usr/bin/env php
<?php
/**
 * Check Google Web Risk API
 * @var $url    The URL to check
 */

$url = $argv[1];
$tokens = file_get_contents('hacked-website-fix-671b675319d7.json');

$token = exec('export GOOGLE_APPLICATION_CREDENTIALS="' . __DIR__ . '/hacked-website-fix-671b675319d7.json"; gcloud auth application-default print-access-token');


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://webrisk.googleapis.com/v1/uris:search?threatTypes=MALWARE&threatTypes=SOCIAL_ENGINEERING&threatTypes=UNWANTED_SOFTWARE&uri=' . $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Authorization: Bearer $token",
));

$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result, true);

// Check if there's any errors
if(isset($result['error'])) {
  echo "API error";
  exit(4);
}

if(count($result) === 0) {
  echo 'No threats found';
  exit(0);
} else {
  $threats = implode(' / ', $result['threat']['threatTypes']);
  echo($threats);
  exit(2);
}
